const express = require('express');

const app = express();

app.set('port', process.env.PORT || 3001);

app.get('/', function (req, res) {
    res.send('Hello World Danilson')
  })

app.get('/estudiantes-palocabildo', function (req, res) {
    res.send('Bienvenidos Estudiantes')
})

app.get('/ayuda', function (req, res) {
    res.send('Esta es la sección de ayuda.')
})

// app.listen(3000)

// console.log('Servidor ejecutandose.')

//Iniciando el servidor
app.listen(app.get('port'),()=>{
    console.log(`Servidor ejecutandose en el puerto ${app.get('port')}`);
});