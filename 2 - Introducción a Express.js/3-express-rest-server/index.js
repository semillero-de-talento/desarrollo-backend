const express = require('express');

const app = express();

app.set('port', process.env.PORT || 3000);
app.set('json spaces', 2)

app.get('/', (req, res) => {
    res.json(
        {
            "Title": "Hola mundo",
            "description": "en un mundo ideal"
        }
    );
})

app.get('/users/getAll', (req, res) => {
    res.json(
        {
            "users": [
                {
                    "Title": "Hola mundo",
                    "description": "en un mundo ideal"
                },
                {
                    "Title": "Hola mundo",
                    "description": "en un mundo ideal"
                }
            ]
        }
    );
})

app.post('/users/get', (req, res) => {
    res.json(
        {
            "users": [
                {
                    "Title": "Hola mundo",
                    "description": "en un mundo ideal"
                },
                {
                    "Title": "Hola mundo",
                    "description": "en un mundo ideal"
                },
                {
                    "Title": "Hola mundo",
                    "description": "en un mundo ideal"
                }

            ]
        }
    );
})

//Iniciando el servidor
app.listen(app.get('port'), () => {
    console.log(`Servidor ejecutandose en el puerto ${app.get('port')}`);
});