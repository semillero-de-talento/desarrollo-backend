const mongoose = require('../conexion/mongo');
const Estudiante = mongoose.model('Estudiante', {
    id: {
        type: Number,
        required: true,
    },
    nombre: {
        type: String,
        required: true,
    },
    calificacion: {
        type: Number,
        required: true,
        min: 0,
    },
});

module.exports = Estudiante;