const mongoose = require('mongoose');
const opciones = {
  useNewUrlParser: true,
  useUnifiedTopology: true
};
mongoose.connect('mongodb+srv://<username>:<password>@cluster0.agb3r68.mongodb.net/?retryWrites=true&w=majority&appName=AtlasApp', opciones);
module.exports = mongoose;