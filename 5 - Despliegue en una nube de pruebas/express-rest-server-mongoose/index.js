// const mongoose = require('mongoose');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const Estudiante = require('./model/estudiantes');

// Parses the text as url encoded data
app.use(bodyParser.urlencoded({ extended: true }));
 
// Parses the text as json
app.use(bodyParser.json());

app.set('port', process.env.PORT || 3000);
app.set('json spaces', 2)

app.get('/', (req, res) => {
    res.json(
        {
            "Title": "Hola mundo",
            "description": "en un mundo ideal"
        }
    );
})

// crear estudiante
app.post('/create', (req, res) => {

    // definimos variables
    let status = 200;
    let success = true;
    let dataRes = {};
    let message = "Datos almacenados correctamente";

    // prepramos el modelo
    const newStudent = new Estudiante({
        id: req.body.id,
        nombre: req.body.nombre,
        calificacion: req.body.calificacion,
    });

    // realizamos el registro
    newStudent.save().catch((err) =>{
        status = 500;
        message = err;

    });

    // devolvemos el resultado
    res.status(status).json(
        {
            'success': success,
            'data': dataRes,
            'message': message,
        }
    );
})

// actualizar estudiante
app.post('/update', async (req, res) => {

    // definimos variables
    let status = 200;
    let success = true;
    let dataRes = {};
    let message = "Datos actualizados correctamente";

    // preparamos el filtro
    const filter = { _id: req.body.id };
    // const filter = { id: { $eq: req.body.id } };

    // preparamos los datos a actualizar
    const update = { 
        nombre: req.body.nombre,
    };

    // realizamos la actualización
    let doc = await Estudiante.findOneAndUpdate(filter, update, {
        new: true
      });

    // dataRes = doc

    // devolvemos el resultado
    res.status(status).json(
        {
            'success': success,
            'data': doc,
            'message': message,
        }
    );
})

// eliminar estudiante
app.post('/delete', async (req, res) => {

    // definimos variables
    let status = 200;
    let success = true;
    let dataRes = {};
    let message = "Datos eliminados correctamente";

    // preparamos el filtro
    const filter = { _id: req.body.id };

    // realizamos la actualización
    let doc = await Estudiante.deleteOne(filter);

    // devolvemos el resultado
    res.status(status).json(
        {
            'success': success,
            'data': doc,
            'message': message,
        }
    );
})

// consultar todos
app.post('/findall', async (req, res) => {

    // definimos variables
    let status = 200;
    let success = true;
    let dataRes = {};
    let message = "Datos consultados correctamente";

    // realizamos la actualización
    let query = await Estudiante.find();
    // query.getFilter();
    // const doc = query.exec();

    // dataRes = query; 
    // res.send(doc);

    // dataRes = JSON.stringify(doc);

    // devolvemos el resultado
    res.status(status).json(
        {
            'success': success,
            'data': query,
            'message': message,
        }
    );
})

//Iniciando el servidor
app.listen(app.get('port'), () => {
    console.log(`Servidor ejecutandose en el puerto ${app.get('port')}`);
});